<?php
/*
Plugin Name: Frieda Simplify Admin Interface for CB2
Author: Nils Larsen
*/

function frieda_simplify_cb_backend_enqueue_admin_script() {

    // Check if we are on the correct page, creating or editing a cb_timeframe
    global $pagenow;
    if (isset($pagenow) && in_array($pagenow, ["post-new.php", "post.php"])) {
        wp_enqueue_script('frieda-simplify-timeframe', plugins_url('/js/frieda-simplify-timeframe.js', __FILE__), array('jquery'), '1.0', true);
    }
}

add_action('admin_enqueue_scripts', 'frieda_simplify_cb_backend_enqueue_admin_script');

?>
