jQuery(document).ready(function($) {

    // Check if we are on the correct page, creating or editing a cb_timeframe
    if (typeof pagenow !== 'undefined' && pagenow === 'cb_timeframe') {

        // ... and set new default value if we are CREATING a timeframe
        if (window.location.href.indexOf('post-new.php') !== -1) {
            $('.cmb2-id-timeframe-advance-booking-days input').val(60);
        }

        // Disable selector for Allowed User Roles, because we always want all users to be able to book
        $('.cmb2-id-allowed-user-roles').hide();

    } else if (typeof pagenow !== 'undefined' && pagenow === 'cb_location') {
        if (window.location.href.indexOf('post-new.php') !== -1) {
            // Deutschland als Land vorgeben
            $('.cmb2-id--cb-location-country input').val("Deutschland");

            // Zeige Karte als Default
            $('.cmb2-id-loc-showmap input').prop('checked', true);
        }

        // Standort E-Mail (für Buchungsbestätigungen) ausblenden, weil keine solche Mails gesendet werden sollen.
        $('.cmb2-id--cb-location-email').hide();
    } else if (typeof pagenow !== 'undefined' && pagenow === 'cb_item') {
        // E-Mail-Adresse des Artikelbetreuers (für Buchungsbestätigungen) ausblenden, weil keine solche Mails gesendet werden sollen.
        $('.cmb2-id--cb-item-maintainer-email').hide();
    }
});


// Re-Format page for printing (timeframe codes)
jQuery(document).ready(function($) {
    function formatDate(originalDate) {
        const monthAbbreviations = {
            'Januar': '01',
            'Februar': '02',
            'März': '03',
            'April': '04',
            'Mai': '05',
            'Juni': '06',
            'Juli': '07',
            'August': '08',
            'September': '09',
            'Oktober': '10',
            'November': '11',
            'Dezember': '12'
        };

        const parts = originalDate.split(' '); // ["Mi.", "22.", "Januar", "2025"]
        const day = parts[1].replace('.', '').padStart(2, '0'); // Remove dot and pad with zero
        const month = monthAbbreviations[parts[2]]; // Map month to number
        const year = parts[3]; // Year is the last part

        // Format the date as "Mi 22.01.2025"
        return `${parts[0].replace('.', '')} ${day}.${month}.${year}`;
    }

    function customizeForPrinting() {
        // Extract data from the table and create a list of spans
        var spanList = [];

        $('.cmb2-codes-outer-table .cmb2-codes-column tr').each(function(index, element) {
            var date = $(element).find('td:first-child').text().trim();
            var code = $(element).find('td:last-child').text().trim();

            if (date && code) {
                spanList.push('<div class="date">' + formatDate(date) + '</div><div class="code">' + code + '</div>');
            }
        });

        // Wrap the combined HTML in a container element
        var combinedHTML = '<div class="combined-container">' + spanList.join('') + '</div>';

        var styleBlock = `
        <style>
        .combined-container div {
            display: inline-block;
            width: 9em;
            margin: 0;

        }
        .date {
            width: 7em !important;
            margin-right: 0.0em;
        }
        </style>`;

        var pageTitle = $('#title').val();

        var fullHTML = `
            <!DOCTYPE html>
            <html lang="en">
            <head>
                <meta charset="UTF-8">
                <meta name="viewport" content="width=device-width, initial-scale=1.0">
                ${styleBlock}
            </head>
            <body>
                <h1>${pageTitle}</h1>
                ${combinedHTML}
            </body>
            </html>
        `;

        originalHtml = $('html').html()
        // Replace the HTML of the whole page with the list of spans
        $('html').html(fullHTML);
    }

    var originalHtml = '';

    // Function to restore visibility of initially visible elements
    function restoreVisibility() {
        $('html').html(originalHtml);
    }

    window.addEventListener('beforeprint', function() {
        customizeForPrinting();
    });

    window.addEventListener('afterprint', function() {
        restoreVisibility();
    });

});
